<html><head>
      <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
   <title>10.4.&nbsp;Implementing custom FlowHandlers</title><link rel="stylesheet" href="css/stylesheet.css" type="text/css"><meta name="generator" content="DocBook XSL Stylesheets V1.74.0"><link rel="home" href="index.html" title="Spring Web Flow Reference Guide"><link rel="up" href="ch10.html" title="10.&nbsp;Spring MVC Integration"><link rel="prev" href="ch10s03.html" title="10.3.&nbsp;Dispatching to flows"><link rel="next" href="ch10s05.html" title="10.5.&nbsp;View Resolution"><!--Begin Google Analytics code--><script type="text/javascript">
			var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
			document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
		</script><script type="text/javascript">
			var pageTracker = _gat._getTracker("UA-2728886-3");
			pageTracker._setDomainName("none");
			pageTracker._setAllowLinker(true);
			pageTracker._trackPageview();
		</script><!--End Google Analytics code--></head><body bgcolor="white" text="black" link="#0000FF" vlink="#840084" alink="#0000FF"><div class="navheader"><table width="100%" summary="Navigation header"><tr><th colspan="3" align="center">10.4.&nbsp;Implementing custom FlowHandlers</th></tr><tr><td width="20%" align="left"><a accesskey="p" href="ch10s03.html">Prev</a>&nbsp;</td><th width="60%" align="center">10.&nbsp;Spring MVC Integration</th><td width="20%" align="right">&nbsp;<a accesskey="n" href="ch10s05.html">Next</a></td></tr></table><hr></div><div class="sect1" lang="en"><div class="titlepage"><div><div><h2 class="title" style="clear: both"><a name="spring-mvc-config-flow-handlers"></a>10.4.&nbsp;Implementing custom FlowHandlers</h2></div></div></div><p>
			<code class="code">FlowHandler</code> is the extension point that can be used to customize how flows are executed in a HTTP servlet environment.
			A <code class="code">FlowHandler</code> is used by the <code class="code">FlowHandlerAdapter</code> and is responsible for:
		</p><div class="itemizedlist"><ul type="disc"><li><p>Returning the <code class="code">id</code> of a flow definition to execute</p></li><li><p>Creating the input to pass new executions of that flow as they are started</p></li><li><p>Handling outcomes returned by executions of that flow as they end</p></li><li><p>Handling any exceptions thrown by executions of that flow as they occur</p></li></ul></div><p>
			These responsibilities are illustrated in the definition of the <code class="code">org.springframework.mvc.servlet.FlowHandler</code> interface:
		</p><pre class="programlisting">
<span class="hl-keyword">public</span> <span class="hl-keyword">interface</span> FlowHandler {

    <span class="hl-keyword">public</span> String getFlowId();

    <span class="hl-keyword">public</span> MutableAttributeMap createExecutionInputMap(HttpServletRequest request);

    <span class="hl-keyword">public</span> String handleExecutionOutcome(FlowExecutionOutcome outcome,
        HttpServletRequest request, HttpServletResponse response);

    <span class="hl-keyword">public</span> String handleException(FlowException e,
        HttpServletRequest request, HttpServletResponse response);
}				
		</pre><p>
			To implement a FlowHandler, subclass <code class="code">AbstractFlowHandler</code>.  All these operations are optional, and if not implemented
			the defaults will apply.  You only need to override the methods that you need.  Specifically:
		</p><div class="itemizedlist"><ul type="disc"><li><p>
					Override <code class="code">getFlowId(HttpServletRequest)</code> when the id of your flow cannot be directly derived from the HTTP request.
					By default, the id of the flow to execute is derived from the pathInfo portion of the request URI.
					For example, <code class="code">http://localhost/app/hotels/booking?hotelId=1</code> results in a flow id of <code class="code">hotels/booking</code> by default.
				</p></li><li><p>
					Override <code class="code">createExecutionInputMap(HttpServletRequest)</code> when you need fine-grained control over extracting
					flow input parameters from the HttpServletRequest.  By default, all request parameters are treated as flow input parameters.
				</p></li><li><p>
					Override <code class="code">handleExecutionOutcome</code> when you need to handle specific flow execution outcomes in a custom manner.
					The default behavior sends a redirect to the ended flow's URL to restart a new execution of the flow.
				</p></li><li><p>
					Override <code class="code">handleException</code> when you need fine-grained control over unhandled flow exceptions.
					The default behavior attempts to restart the flow when a client attempts to access an ended or expired flow execution.
					Any other exception is rethrown to the Spring MVC ExceptionResolver infrastructure by default.
				</p></li></ul></div><div class="sect2" lang="en"><div class="titlepage"><div><div><h3 class="title"><a name="spring-mvc-flow-handler-example"></a>Example FlowHandler</h3></div></div></div><p>
				A common interaction pattern between Spring MVC And Web Flow is for a Flow to redirect to a @Controller when it ends.
				FlowHandlers allow this to be done without coupling the flow definition itself with a specific controller URL.
				An example FlowHandler that redirects to a Spring MVC Controller is shown below:
			</p><pre class="programlisting">
<span class="hl-keyword">public</span> <span class="hl-keyword">class</span> BookingFlowHandler <span class="hl-keyword">extends</span> AbstractFlowHandler {
    <span class="hl-keyword">public</span> String handleExecutionOutcome(FlowExecutionOutcome outcome,
                                         HttpServletRequest request, HttpServletResponse response) {
        <span class="hl-keyword">if</span> (outcome.getId().equals(<span class="hl-string">"bookingConfirmed"</span>)) {
            <span class="hl-keyword">return</span> <span class="hl-string">"/booking/show?bookingId="</span> + outcome.getOutput().get(<span class="hl-string">"bookingId"</span>);
        } <span class="hl-keyword">else</span> {
            <span class="hl-keyword">return</span> <span class="hl-string">"/hotels/index"</span>;
        }
    }
}
			</pre><p>
				Since this handler only needs to handle flow execution outcomes in a custom manner, nothing else is overridden.
				The <code class="code">bookingConfirmed</code> outcome will result in a redirect to show the new booking.
				Any other outcome will redirect back to the hotels index page.
			</p></div><div class="sect2" lang="en"><div class="titlepage"><div><div><h3 class="title"><a name="d0e2575"></a>Deploying a custom FlowHandler</h3></div></div></div><p>
				To install a custom FlowHandler, simply deploy it as a bean.
				The bean name must match the id of the flow the handler should apply to.
			</p><pre class="programlisting">
&lt;<span class="hl-tag">bean</span> <span class="hl-attribute">name</span>=<span class="hl-value">"hotels/booking"</span> <span class="hl-attribute">class</span>=<span class="hl-value">"org.springframework.webflow.samples.booking.BookingFlowHandler"</span> /&gt;
			</pre><p>
				With this configuration, accessing the resource <code class="code">/hotels/booking</code> will launch the <code class="code">hotels/booking</code> flow using the custom BookingFlowHandler.
				When the booking flow ends, the FlowHandler will process the flow execution outcome and redirect to the appropriate controller.
			</p></div><div class="sect2" lang="en"><div class="titlepage"><div><div><h3 class="title"><a name="spring-mvc-flow-handler-redirects"></a>FlowHandler Redirects</h3></div></div></div><p>
				A FlowHandler handling a FlowExecutionOutcome or FlowException returns a <code class="code">String</code> to indicate the resource to redirect to after handling.
				In the previous example, the <code class="code">BookingFlowHandler</code> redirects to the <code class="code">booking/show</code> resource URI for <code class="code">bookingConfirmed</code> outcomes,
				and the <code class="code">hotels/index</code> resource URI for all other outcomes. 
			</p><p>
				By default, returned resource locations are relative to the current servlet mapping.
				This allows for a flow handler to redirect to other Controllers in the application using relative paths.
				In addition, explicit redirect prefixes are supported for cases where more control is needed.
			</p><p>
				The explicit redirect prefixes supported are:
			</p><div class="itemizedlist"><ul type="disc"><li><p><code class="code">servletRelative:</code> - redirect to a resource relative to the current servlet</p></li><li><p><code class="code">contextRelative:</code> - redirect to a resource relative to the current web application context path</p></li><li><p><code class="code">serverRelative:</code> - redirect to a resource relative to the server root</p></li><li><p><code class="code">http://</code> or <code class="code">https://</code> - redirect to a fully-qualified resource URI</p></li></ul></div><p>
				These same redirect prefixes are also supported within a flow definition when using the <code class="code">externalRedirect:</code> directive in
				conjunction with a view-state or end-state; for example, <code class="code">view="externalRedirect:http://springframework.org"</code>
			</p></div></div><!--Begin LoopFuse code--><script src="http://loopfuse.net/webrecorder/js/listen.js" type="text/javascript"></script><script type="text/javascript">
			_lf_cid = "LF_48be82fa";
			_lf_remora();
		</script><!--End LoopFuse code--><div class="navfooter"><hr><table width="100%" summary="Navigation footer"><tr><td width="40%" align="left"><a accesskey="p" href="ch10s03.html">Prev</a>&nbsp;</td><td width="20%" align="center"><a accesskey="u" href="ch10.html">Up</a></td><td width="40%" align="right">&nbsp;<a accesskey="n" href="ch10s05.html">Next</a></td></tr><tr><td width="40%" align="left" valign="top">10.3.&nbsp;Dispatching to flows&nbsp;</td><td width="20%" align="center"><a accesskey="h" href="index.html">Home</a></td><td width="40%" align="right" valign="top">&nbsp;10.5.&nbsp;View Resolution</td></tr></table></div></body></html>