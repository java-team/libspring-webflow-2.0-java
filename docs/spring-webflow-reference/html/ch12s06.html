<html><head>
      <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
   <title>12.6.&nbsp;Replacing the JSF Managed Bean Facility</title><link rel="stylesheet" href="css/stylesheet.css" type="text/css"><meta name="generator" content="DocBook XSL Stylesheets V1.74.0"><link rel="home" href="index.html" title="Spring Web Flow Reference Guide"><link rel="up" href="ch12.html" title="12.&nbsp;JSF Integration"><link rel="prev" href="ch12s05.html" title="12.5.&nbsp;Configuring faces-config.xml"><link rel="next" href="ch12s07.html" title="12.7.&nbsp;Handling JSF Events With Spring Web Flow"><!--Begin Google Analytics code--><script type="text/javascript">
			var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
			document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
		</script><script type="text/javascript">
			var pageTracker = _gat._getTracker("UA-2728886-3");
			pageTracker._setDomainName("none");
			pageTracker._setAllowLinker(true);
			pageTracker._trackPageview();
		</script><!--End Google Analytics code--></head><body bgcolor="white" text="black" link="#0000FF" vlink="#840084" alink="#0000FF"><div class="navheader"><table width="100%" summary="Navigation header"><tr><th colspan="3" align="center">12.6.&nbsp;Replacing the JSF Managed Bean Facility</th></tr><tr><td width="20%" align="left"><a accesskey="p" href="ch12s05.html">Prev</a>&nbsp;</td><th width="60%" align="center">12.&nbsp;JSF Integration</th><td width="20%" align="right">&nbsp;<a accesskey="n" href="ch12s07.html">Next</a></td></tr></table><hr></div><div class="sect1" lang="en"><div class="titlepage"><div><div><h2 class="title" style="clear: both"><a name="spring-faces-managed-beans"></a>12.6.&nbsp;Replacing the JSF Managed Bean Facility</h2></div></div></div><p>
            Spring Faces allows you to completely replace the JSF managed bean facility with a combination of
            flow-managed variables and Spring managed beans. It gives you a good deal more control over the lifecycle of
            your managed objects with well-defined hooks for initialization and execution of your domain model.
            Additionally, since you are presumably already using Spring for your business layer, it reduces the
            conceptual overhead of having to maintain two different managed bean models.
        </p><p>
            In doing pure JSF development, you will quickly find that request scope is not long-lived enough for storing
            conversational model objects that drive complex event-driven views. The only available option is to begin
            putting things into session scope, with the extra burden of needing to clean the objects up before
            progressing to another view or functional area of the application. What is really needed is a managed scope
            that is somewhere between request and session scope. Fortunately web flow provides such extended facilities.
        </p><div class="sect2" lang="en"><div class="titlepage"><div><div><h3 class="title"><a name="spring-faces-flow-variables"></a>Using Flow Variables</h3></div></div></div><p>
                The easiest and most natural way to declare and manage the model is through the use of
                <a class="link" href="ch02s08.html" title="2.8.&nbsp;Variables">flow variables</a>
                . You can declare these variables at the beginning of the flow:
            </p><pre class="programlisting">
&lt;<span class="hl-tag">var</span> <span class="hl-attribute">name</span>=<span class="hl-value">"searchCriteria"</span> <span class="hl-attribute">class</span>=<span class="hl-value">"com.mycompany.myapp.hotels.search.SearchCriteria"</span>/&gt;
            </pre><p>and then reference this variable in one of the flow's JSF view templates through EL:</p><pre class="programlisting">
&lt;<span class="hl-tag">h:inputText</span> <span class="hl-attribute">id</span>=<span class="hl-value">"searchString"</span> <span class="hl-attribute">value</span>=<span class="hl-value">"#{searchCriteria.searchString}"</span>/&gt;
            </pre><p>
                Note that you do not need to prefix the variable with its scope when referencing it from the template
                (though you can do so if you need to be more specific). As with standard JSF beans, all available scopes
                will be searched for a matching variable, so you could change the scope of the variable in your flow
                definition without having to modify the EL expressions that reference it.
            </p><p>
                You can also define view instance variables that are scoped to the current view and get cleaned up
                automatically upon transitioning to another view. This is quite useful with JSF as views are often
                constructed to handle multiple in-page events across many requests before transitioning to another view.
            </p><p>
                To define a view instance variable, you can use the
                <code class="code">var</code>
                element inside a
                <code class="code">view-state</code>
                definition:
            </p><pre class="programlisting">
&lt;<span class="hl-tag">view-state</span> <span class="hl-attribute">id</span>=<span class="hl-value">"enterSearchCriteria"</span>&gt; 
    &lt;<span class="hl-tag">var</span> <span class="hl-attribute">name</span>=<span class="hl-value">"searchCriteria"</span> <span class="hl-attribute">class</span>=<span class="hl-value">"com.mycompany.myapp.hotels.search.SearchCriteria"</span>/&gt; 
&lt;<span class="hl-tag">/view-state</span>&gt;
            </pre></div><div class="sect2" lang="en"><div class="titlepage"><div><div><h3 class="title"><a name="spring-faces-spring-beans"></a>Using Scoped Spring Beans</h3></div></div></div><p>
                Though defining autowired flow instance variables provides nice modularization and readability,
                occasions may arise where you want to utilize the other capabilities of the Spring container such as
                AOP. In these cases, you can define a bean in your Spring ApplicationContext and give it a specific web
                flow scope:
            </p><pre class="programlisting">
&lt;<span class="hl-tag">bean</span> <span class="hl-attribute">id</span>=<span class="hl-value">"searchCriteria"</span> <span class="hl-attribute">class</span>=<span class="hl-value">"com.mycompany.myapp.hotels.search.SearchCriteria"</span> <span class="hl-attribute">scope</span>=<span class="hl-value">"flow"</span>/&gt;
            </pre><p>
                The major difference with this approach is that the bean will not be fully initialized until it is first
                accessed via an EL expression. This sort of lazy instantiation via EL is quite similar to how JSF
                managed beans are typically allocated.
            </p></div><div class="sect2" lang="en"><div class="titlepage"><div><div><h3 class="title"><a name="faces-manipulating-model"></a>Manipulating The Model</h3></div></div></div><p>
                The need to initialize the model before view rendering (such as by loading persistent entities from a
                database) is quite common, but JSF by itself does not provide any convenient hooks for such
                initialization. The flow definition language provides a natural facility for this through its
                <a class="link" href="ch02s06.html" title="2.6.&nbsp;Actions">Actions</a>
                . Spring Faces provides some extra conveniences for converting the outcome of an action into a
                JSF-specific data structure. For example:
            </p><pre class="programlisting"> 
&lt;<span class="hl-tag">on-render</span>&gt;
    &lt;<span class="hl-tag">evaluate</span> <span class="hl-attribute">expression</span>=<span class="hl-value">"bookingService.findBookings(currentUser.name)"</span> 
              <span class="hl-attribute">result</span>=<span class="hl-value">"viewScope.bookings"</span> <span class="hl-attribute">result-type</span>=<span class="hl-value">"dataModel"</span> /&gt;
&lt;<span class="hl-tag">/on-render</span>&gt;
            </pre><p>
                This will take the result of the
                <code class="code">bookingService.findBookings</code>
                method an wrap it in a custom JSF DataModel so that the list can be used in a standard JSF DataTable
                component:
            </p><pre class="programlisting"> 
&lt;<span class="hl-tag">h:dataTable</span> <span class="hl-attribute">id</span>=<span class="hl-value">"bookings"</span> <span class="hl-attribute">styleClass</span>=<span class="hl-value">"summary"</span> <span class="hl-attribute">value</span>=<span class="hl-value">"#{bookings}"</span> <span class="hl-attribute">var</span>=<span class="hl-value">"booking"</span> 
             <span class="hl-attribute">rendered</span>=<span class="hl-value">"#{bookings.rowCount &gt; 0}"</span>&gt;
    &lt;<span class="hl-tag">h:column</span>&gt;
        &lt;<span class="hl-tag">f:facet</span> <span class="hl-attribute">name</span>=<span class="hl-value">"header"</span>&gt;Name&lt;<span class="hl-tag">/f:facet</span>&gt;
        #{booking.hotel.name}
    &lt;<span class="hl-tag">/h:column</span>&gt;                   
    &lt;<span class="hl-tag">h:column</span>&gt;
    &lt;<span class="hl-tag">f:facet</span> <span class="hl-attribute">name</span>=<span class="hl-value">"header"</span>&gt;Confirmation number&lt;<span class="hl-tag">/f:facet</span>&gt;
        #{booking.id}
        &lt;<span class="hl-tag">/h:column</span>&gt;
    &lt;<span class="hl-tag">h:column</span>&gt;
        &lt;<span class="hl-tag">f:facet</span> <span class="hl-attribute">name</span>=<span class="hl-value">"header"</span>&gt;Action&lt;<span class="hl-tag">/f:facet</span>&gt;
        &lt;<span class="hl-tag">h:commandLink</span> <span class="hl-attribute">id</span>=<span class="hl-value">"cancel"</span> <span class="hl-attribute">value</span>=<span class="hl-value">"Cancel"</span> <span class="hl-attribute">action</span>=<span class="hl-value">"cancelBooking"</span> /&gt;
    &lt;<span class="hl-tag">/h:column</span>&gt;
&lt;<span class="hl-tag">/h:dataTable</span>&gt;
            </pre><p>
                The custom DataModel provides some extra conveniences such as being serializable for storage beyond
                request scope and access to the currently selected row in EL expressions. For example, on postback from
                a view where the action event was fired by a component within a DataTable, you can take action on the
                selected row's model instance:
            </p><pre class="programlisting">
&lt;<span class="hl-tag">transition</span> <span class="hl-attribute">on</span>=<span class="hl-value">"cancelBooking"</span>&gt;
    &lt;<span class="hl-tag">evaluate</span> <span class="hl-attribute">expression</span>=<span class="hl-value">"bookingService.cancelBooking(bookings.selectedRow)"</span> /&gt;            
&lt;<span class="hl-tag">/transition</span>&gt;
            </pre></div></div><!--Begin LoopFuse code--><script src="http://loopfuse.net/webrecorder/js/listen.js" type="text/javascript"></script><script type="text/javascript">
			_lf_cid = "LF_48be82fa";
			_lf_remora();
		</script><!--End LoopFuse code--><div class="navfooter"><hr><table width="100%" summary="Navigation footer"><tr><td width="40%" align="left"><a accesskey="p" href="ch12s05.html">Prev</a>&nbsp;</td><td width="20%" align="center"><a accesskey="u" href="ch12.html">Up</a></td><td width="40%" align="right">&nbsp;<a accesskey="n" href="ch12s07.html">Next</a></td></tr><tr><td width="40%" align="left" valign="top">12.5.&nbsp;Configuring faces-config.xml&nbsp;</td><td width="20%" align="center"><a accesskey="h" href="index.html">Home</a></td><td width="40%" align="right" valign="top">&nbsp;12.7.&nbsp;Handling JSF Events With Spring Web Flow</td></tr></table></div></body></html>