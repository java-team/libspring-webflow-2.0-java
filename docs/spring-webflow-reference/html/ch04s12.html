<html><head>
      <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
   <title>4.12.&nbsp;Executing view transitions</title><link rel="stylesheet" href="css/stylesheet.css" type="text/css"><meta name="generator" content="DocBook XSL Stylesheets V1.74.0"><link rel="home" href="index.html" title="Spring Web Flow Reference Guide"><link rel="up" href="ch04.html" title="4.&nbsp;Rendering views"><link rel="prev" href="ch04s11.html" title="4.11.&nbsp;Suppressing validation"><link rel="next" href="ch04s13.html" title="4.13.&nbsp;Working with messages"><!--Begin Google Analytics code--><script type="text/javascript">
			var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
			document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
		</script><script type="text/javascript">
			var pageTracker = _gat._getTracker("UA-2728886-3");
			pageTracker._setDomainName("none");
			pageTracker._setAllowLinker(true);
			pageTracker._trackPageview();
		</script><!--End Google Analytics code--></head><body bgcolor="white" text="black" link="#0000FF" vlink="#840084" alink="#0000FF"><div class="navheader"><table width="100%" summary="Navigation header"><tr><th colspan="3" align="center">4.12.&nbsp;Executing view transitions</th></tr><tr><td width="20%" align="left"><a accesskey="p" href="ch04s11.html">Prev</a>&nbsp;</td><th width="60%" align="center">4.&nbsp;Rendering views</th><td width="20%" align="right">&nbsp;<a accesskey="n" href="ch04s13.html">Next</a></td></tr></table><hr></div><div class="sect1" lang="en"><div class="titlepage"><div><div><h2 class="title" style="clear: both"><a name="view-transitions"></a>4.12.&nbsp;Executing view transitions</h2></div></div></div><p>
			Define one or more <code class="code">transition</code> elements to handle user events that may occur on the view.
			A transition may take the user to another view, or it may simply execute an action and re-render the current view.
			A transition may also request the rendering of parts of a view called "fragments" when handling an Ajax event.
			Finally, "global" transitions that are shared across all views may also be defined.
		</p><p>
			Implementing view transitions is illustrated in the following sections.
		</p><div class="sect2" lang="en"><div class="titlepage"><div><div><h3 class="title"><a name="transition-actions"></a>Transition actions</h3></div></div></div><p>
				A view-state transition can execute one or more actions before executing.
				These actions may return an error result to prevent the transition from exiting the current view-state.
				If an error result occurs, the view will re-render and should display an appropriate message to the user.
			</p><p>
				If the transition action invokes a plain Java method, the invoked method may return false to prevent the transition from executing.
				This technique can be used to handle exceptions thrown by service-layer methods.
				The example below invokes an action that calls a service and handles an exceptional situation:
			</p><pre class="programlisting">
&lt;<span class="hl-tag">transition</span> <span class="hl-attribute">on</span>=<span class="hl-value">"submit"</span> <span class="hl-attribute">to</span>=<span class="hl-value">"bookingConfirmed"</span>&gt;
    &lt;<span class="hl-tag">evaluate</span> <span class="hl-attribute">expression</span>=<span class="hl-value">"bookingAction.makeBooking(booking, messageContext)"</span> /&gt;
&lt;<span class="hl-tag">/transition</span>&gt;
			</pre><pre class="programlisting">
<span class="hl-keyword">public</span> <span class="hl-keyword">class</span> BookingAction {
   <span class="hl-keyword">public</span> <span class="hl-keyword">boolean</span> makeBooking(Booking booking, MessageContext context) {
       <span class="hl-keyword">try</span> {
           bookingService.make(booking);
           <span class="hl-keyword">return</span> true;
       } <span class="hl-keyword">catch</span> (RoomNotAvailableException e) {
           context.addMessage(<span class="hl-keyword">new</span> MessageBuilder().error().
               .defaultText(<span class="hl-string">"No room is available at this hotel"</span>).build());
           <span class="hl-keyword">return</span> false;
       }
   }
}
			</pre><div class="note" style="margin-left: 0.5in; margin-right: 0.5in;"><table border="0" summary="Note"><tr><td rowspan="2" align="center" valign="top" width="25"><img alt="[Note]" src="images/note.gif"></td><th align="left">Note</th></tr><tr><td align="left" valign="top"><p>
					When there is more than one action defined on a transition, if one returns an error result the remaining actions in the set will <span class="emphasis"><em>not</em></span> be executed.
					If you need to ensure one transition action's result cannot impact the execution of another, define a single transition action that invokes a method that encapsulates all the action logic.
				</p></td></tr></table></div></div><div class="sect2" lang="en"><div class="titlepage"><div><div><h3 class="title"><a name="event-handlers-global"></a>Global transitions</h3></div></div></div><p>
				Use the flow's <code class="code">global-transitions</code> element to create transitions that apply across all views.
				Global-transitions are often used to handle global menu links that are part of the layout.
			</p><pre class="programlisting">
&lt;<span class="hl-tag">global-transitions</span>&gt;
    &lt;<span class="hl-tag">transition</span> <span class="hl-attribute">on</span>=<span class="hl-value">"login"</span> <span class="hl-attribute">to</span>=<span class="hl-value">"login"</span> /&gt;
    &lt;<span class="hl-tag">transition</span> <span class="hl-attribute">on</span>=<span class="hl-value">"logout"</span> <span class="hl-attribute">to</span>=<span class="hl-value">"logout"</span> /&gt;
&lt;<span class="hl-tag">/global-transitions</span>&gt;
			</pre></div><div class="sect2" lang="en"><div class="titlepage"><div><div><h3 class="title"><a name="simple-event-handlers"></a>Event handlers</h3></div></div></div><p>
				From a view-state, transitions without targets can also be defined.  Such transitions are called "event handlers":
			</p><pre class="programlisting">
&lt;<span class="hl-tag">transition</span> <span class="hl-attribute">on</span>=<span class="hl-value">"event"</span>&gt;
    &lt;<span class="hl-comment">!-- Handle event --</span>&gt;
&lt;<span class="hl-tag">/transition</span>&gt;
			</pre><p>
				These event handlers do not change the state of the flow.
				They simply execute their actions and re-render the current view or one or more fragments of the current view.
			</p></div><div class="sect2" lang="en"><div class="titlepage"><div><div><h3 class="title"><a name="event-handlers-render"></a>Rendering fragments</h3></div></div></div><p>
				Use the <code class="code">render</code> element within a transition to request partial re-rendering of the current view after handling the event:
			</p><pre class="programlisting">
&lt;<span class="hl-tag">transition</span> <span class="hl-attribute">on</span>=<span class="hl-value">"next"</span>&gt;
    &lt;<span class="hl-tag">evaluate</span> <span class="hl-attribute">expression</span>=<span class="hl-value">"searchCriteria.nextPage()"</span> /&gt;
    &lt;<span class="hl-tag">render</span> <span class="hl-attribute">fragments</span>=<span class="hl-value">"searchResultsFragment"</span> /&gt;            
&lt;<span class="hl-tag">/transition</span>&gt;
			</pre><p>
				The fragments attribute should reference the id(s) of the view element(s) you wish to re-render.
				Specify multiple elements to re-render by separating them with a comma delimiter.
			</p><p>
				Such partial rendering is often used with events signaled by Ajax to update a specific zone of the view.
			</p></div></div><!--Begin LoopFuse code--><script src="http://loopfuse.net/webrecorder/js/listen.js" type="text/javascript"></script><script type="text/javascript">
			_lf_cid = "LF_48be82fa";
			_lf_remora();
		</script><!--End LoopFuse code--><div class="navfooter"><hr><table width="100%" summary="Navigation footer"><tr><td width="40%" align="left"><a accesskey="p" href="ch04s11.html">Prev</a>&nbsp;</td><td width="20%" align="center"><a accesskey="u" href="ch04.html">Up</a></td><td width="40%" align="right">&nbsp;<a accesskey="n" href="ch04s13.html">Next</a></td></tr><tr><td width="40%" align="left" valign="top">4.11.&nbsp;Suppressing validation&nbsp;</td><td width="20%" align="center"><a accesskey="h" href="index.html">Home</a></td><td width="40%" align="right" valign="top">&nbsp;4.13.&nbsp;Working with messages</td></tr></table></div></body></html>